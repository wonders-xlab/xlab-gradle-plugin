# 使得项目变得适应于xlab环境的gradle plugin
## 简单使用
在项目的build.gradle文件中加入关于build的源和依赖，并apply插件，随后调用 useEverything()。Now everything works like a charm.

```groovy
buildscript {
  repositories { maven { url "http://xlab-tech.com:45682/nexus/content/groups/public/" } }
  dependencies { classpath("com.wonders.xlab:gradle-plugin:$pluginNumber") }
}
apply plugin: 'xlab-gradle-plugin'
useEverything()
```

> 如果想要变得更帅气，更可以用 **xlabified()** 来替代 useEverything()

```groovy
apply plugin: 'xlab-gradle-plugin'
xlabified()
```
> 你也可以使用using(List\<String\>)方法来传入企图开启的部分功能，这种方法最为灵活。

```groovy
apply plugin: 'xlab-gradle-plugin'
using(["xlabDependencies", "copyDependencies", "gitBatchPush"])
```
## feature 列表
> 所有feature均可通过调用 use*${feature名首字母大写}*() 方法来单独启用。（例如需要打开名为xlabProfiling的“为项目添加基于profile的构建配置”的feature，则调用 useXlabProfiling()即可）useEverything() 和 xlabified() 则会开启所有功能。

+ xlabMavenRepositories
> 为项目依赖添加 mavenCentral 和位于 [http://www.xlab-tech.com:45682/nexus/content/groups/public/](http://www.xlab-tech.com:45682/nexus/content/groups/public/) 的nexus的repository

+ xlabDependencies   
>>>
为项目添加常见依赖，包括：    
1.  sl4j-api，仅包含封装接口，具体实现可以在不同的Test或JavaEE环境下以runtime configuration来提供，方便日志服务的一致化。    
2.  如果项目为groovy项目。（即build.gradle中调用了apply plugin: 'groovy'）则自动添加org.codehaus.groovy:groovy-all的compile依赖，而无需再次手动引入。    
3.  如果项目为scala项目。（即build.gradle中调用了apply plugin: 'scala'）则自动添加org.scala-lang:scala-library的compile依赖，而无需再次手动引入。     

可以通过在初始化feature之前，给名为scalaVersion和groovyVersion的变量赋值来制定library版本，若不这么做，则会自动引入最新版（即"+"）。
>>>
```groovy
ext.groovyVersion = "2.4.7" //赋值语句
apply plugin: "groovy"
apply plugin: 'xlab-gradle-plugin' 
xlabified() //对version的赋值必须在这一句之前
```
```groovy
apply plugin: "scala"
ext.scalaVersion = "2.12.1" //赋值语句
apply plugin: 'xlab-gradle-plugin' 
xlabified() //对version的赋值必须在这一句之前
```

+   xlabTesting
> 添加了testCompile下的一些依赖，以及testRuntime下的slf4j-jdk14依赖以进行简单的test日志打印。apply了jacoco的gradle插件，方便生成默认的coverage报告。惊醒少许自定义设置。具体参见实际代码。


+   xlabProfiling
> 添加了基于profile的构建配置脚本支持。为项目构建的project scope添加变量profile，默认的项目profile种类有三种：DEVELOPMENT; TESTING; PRODUCTION; profile变量为字符串类型。由于profile变量名常用，因此添加了静态字符串常量PROFILE_TAG="profile"。另在同作用域下为每个profile种类添加了helper方法，例如：testMode()、developMode() 和 productMode()，返回值皆为boolean，用来较为直观地判断当次构建所属的profile种类。传入profile参数的方法如下：
```groovy
gradle build -Pprofile=testing
gradle bootRun -Pprofile=development
```
> 同时支持自定义profile种类，在调用useXlabProfiling()之前，给名为definedProfile的变量赋值以创建自定义profile种类，该变量的类型为Set\<String\>。（但无论传入值是否包括默认种类，默认的三种种类都会被添加和支持）
```groovy
apply plugin: 'xlab-gradle-plugin'
ext.definedProfile = ["a","z","az", "azzaZhiXinNianGao"]
xlabified()
println aMode()
```
```groovy
gradle build -Pprofile=a //prints true
gradle build //prints false
```
+   xlabMavenPlublish
> 为项目apply了maven-publish插件，使用gradle publish任务可以自动将项目打包发布至xlab的nexus maven repository（目前仅支持jar的发布）。


+   copyDependencies
> 为项目的每个configuration添加了名为 copy${configuration名首字母大写} Libs 的任务，执行之则会拷贝该configuration下的所有dependencies到_buildDir/lib/dependencies/$configuration名_目录下，若项目已打包，则项目本身的包也会被拷入，方便进行依赖分析、优化和审核。

+   xlabDependencyScope
> 为项目的dependencies域添加了一种依赖形式（就像gradle war plugin添加了providedCompile和providedRuntime）：xlabCompile。其作用方式很简单，使用gradle多项目构建。如下配置：
```groovy
dependencies {
  xlabCompile "xlab-service"
}
```
将被转化为：
当处于devepment profile下，引用同一构建root下的另一个具有相应名称子项目（一般表现为idea中的同名module），等价于：
```groovy
dependencies {
  compile project(":xlab-service")
}
```
当处于其他profile下，则query xlab的nexus maven repositroy，并根据特定规则寻找源内发布的jar包，等价于：
```groovy
dependencies {
  compile "com.wonders.xlab:xlab-service:+"
}
```
>> 语法即简单地为： xlabCompile ${项目或包名字（两者必须相同）}。
>> 该feature必须同时启用上述xlabProfiling功能才会生效，否则不会起效但可能出现报错。
>> 同样添加了xlabRuntime、xlabTestCompile和xlabTestRuntime，若项目apply了war插件，则会添加xlabProvidedCompile和xlabProvidedRuntime。
+   gitBatchPush
> 若项目是git repository并具有多个remote，则提供新的名为gradle pushAll的task一次性push所有分支到所有远端。同时提供名为remote的参数，以$remoteName1,$remoteName2...的形式传入，以逗号分割。若传入该参数则会只push到传入的远端。若不传入则会push所有远端。
```groovy
gradle pushAll -Premote=gitlab,origin --refresh-dependencies //pushing to gitlab and origin
gradle pushAll --refresh-dependencies //pushing to every remote
```
