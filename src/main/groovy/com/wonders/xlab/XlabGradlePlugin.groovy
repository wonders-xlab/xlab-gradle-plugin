package com.wonders.xlab

import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.Copy

/**
 * Created by xiamubobby on 22/12/2016.
 */
//@TypeChecked
class XlabGradlePlugin implements Plugin<Project> {
    void apply(Project project) {

        //function of acquiring xlab maven nexus url
        def resolveXlabNexusDomain = {
            String ip = ""
            try {
                URL whatismyip = new URL("http://checkip.amazonaws.com")
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                        whatismyip.openStream()))
                ip = bufferedReader.readLine()
                println(ip)
            } catch (Exception ignored) {

            }
            String ret = "http://${ip.startsWith("210.13") ? "10.1.64.178:8081" : "101.231.124.8:45682"}"
            println(ret)
            return ret
        }
        project.ext.resolveXlabNexusDomain = resolveXlabNexusDomain

        //copyDependencies tasks for evey configuration
        project.ext.useCopyDependencies = {
            for (configuration in project.configurations) {
                project.task(type: Copy, "copy${configuration.name.capitalize()}Libs") {
                    group = 'Dependency Analyzing'
                    if (project.plugins.hasPlugin('java')
                            && project.jar.outputs.files.singleFile.exists())
                        from project.jar.outputs.files.singleFile
                    from configuration
                    into "${project.buildDir}/lib/dependencies/${configuration.name}"
                    description "Copies the dependencies of ${configuration.name.capitalize()} configuration"
                }
            }
        }

        //add mavenCentral and xlab maven nexus to the project
        project.ext.useXlabMavenRepositories = {
            project.repositories {
                mavenCentral()
            }
            project.repositories {
                maven { url "${resolveXlabNexusDomain()}/nexus/content/groups/public/" }
            }
        }

        //add profile support
        project.ext.useXlabProfiling = {
            project.ext.usingXlabProfiling = true
            def DEFINED_PROFILE_TAG = "definedProfile"
            project.ext.DEFINED_PROFILE_TAG = DEFINED_PROFILE_TAG
//            def configGroovy = new File("${project.projectDir}/buildSrc/config.groovy")
//            if (!configGroovy.exists()) {
//                configGroovy.parentFile.mkdirs()
//                configGroovy.createNewFile()
//                configGroovy.withWriter {writer ->
//                    writer.append('environments {\n')
//                    for(profileName in ["testing", "development", "production"]) {
//                        writer.append('\t').append("$profileName {\n\n\t}\n")
//                    }
//                    writer.append('}')
//                }
//            } else {
//                Set<String> profilesInConfig = new HashSet<>()
//                def inProfile = false
//                int level = 0
//                List<String> tags = []
//                def lines = configGroovy.readLines()
//                for (line in lines) {
//                    def left = 0
//                    if(!line.isEmpty()) {
//                        0.upto(line.getChars().size() - 1) {
//                            def chara = line.getChars()[it]
//                            if (chara == ('{' as char)) {
//                                def matcher = line.subSequence(left, it + 1) =~ /(?<=\s|^)\S*(?=\s*\{)/
//                                matcher.find()
//                                tags.add(matcher.group())
//                                tags.add("condition : ${level == 1} : ${tags[tags.size() - 2]}")
//                                if (level == 1 && (tags[tags.size() - 2] == "environments")) {
//                                    println("really adds")
//                                    profilesInConfig.add(tags[tags.size() - 1])
//                                }
//                                level++
//                            }
//                            if (chara == ('}' as char)) {
//                                level--
//                                left = it
//                            }
//                        }
//                    }
//                }
//                println("set : ${profilesInConfig.size()}")
            Set<String> definedProfiles = ["testing", "development", "production"]
//            if (project.hasProperty(DEFINED_PROFILE_TAG)) {
//                definedProfiles = definedProfiles + (project.property(DEFINED_PROFILE_TAG) as Set)
//            }
            project.ext.definedProfiles = definedProfiles
            String PROFILE_TAG = "profile"
            project.ext.PROFILE_TAG = PROFILE_TAG
            def profile = Profile.DEVELOPMENT.name().toLowerCase()
            if (project.hasProperty(PROFILE_TAG)) {
                profile = (project.property(PROFILE_TAG)).toLowerCase()
                if (!(profile in project.ext.definedProfiles)) {
                    throw new IllegalArgumentException("用户指定了一个不合法的Profile:$profile in ${project.ext.definedProfiles}")
                }
            }
            project.ext.profile = profile
            for (String name in project.ext.definedProfiles) {
                String delegateName = name
                project.ext."${delegateName.toLowerCase()}Mode" = {
                    profile.toLowerCase() == delegateName.toLowerCase()
                }
            }
        }

        //add config.groovy support
        project.ext.useConfigGroovy = {
            String CONFIGS = "configs"
            project.ext.CONFIGS = CONFIGS
            List<String> definedConfigs = []
            project.ext.definedConfigs = definedConfigs
            Map<String, String> configeds = [:]
            project.ext.configeds = configeds
            ConfigSlurper configSlurper = new ConfigSlurper(project.profile)
            project.ext.definedConfigs.each {
                if (project.hasProperty(it)) {
                    project.ext.configeds.put(it, project.property(it))
                }
            }
            project.ext.configeds.each { k, v ->
                configSlurper.registerConditionalBlock(k, v)
            }
            project.ext.configs = configSlurper.parse(project.file("buildSrc/config.groovy").toURI().toURL())
            project.processResources {
                project.with {
                    delete "${project.buildDir}/resources/main/"
                    from(sourceSets.main.resources.srcDirs) {
                        filter(ReplaceTokens, tokens: configs.toProperties())
                    }
                }
            }
        }

        //add test coverage
        project.ext.useXlabTesting = {
            project.apply plugin: 'jacoco'
            project.jacocoTestReport {
                reports {
                    xml.destination "${project.buildDir}/jacocoXml"
                    csv.enabled false
                    html.destination "${project.buildDir}/jacocoHtml"
                }
            }
            project.dependencies {
                testCompile group: 'junit', name: 'junit', version: '4.11'
                testCompile 'org.scalatest:scalatest_2.11:3.0.1'
                testRuntime 'org.slf4j:slf4j-jdk14:1.7.22'
                testCompile 'com.shazam:shazamcrest:0.11'
            }
        }

        //add common xlab dependencies to project
        project.ext.useXlabDependencies = {
            project.dependencies {
                compile('org.slf4j:slf4j-api:1.7.22')
                compile group: 'com.intellij', name: 'annotations', version: '12.0'
            }
            project.afterEvaluate {
                if (project.plugins.hasPlugin('scala')) {
                    project.dependencies {
                        compile "org.scala-lang:scala-library:${project.hasProperty("scalaVersion") ? project.ext.scalaVersion : "+"}"
                    }
                }
                if (project.plugins.hasPlugin('groovy')) {
                    project.dependencies {
                        compile "org.codehaus.groovy:groovy-all:${project.hasProperty("groovyVersion") ? project.ext.groovyVersion : "+"}"
                    }
                }
            }
        }

        //add xlab maven publishing feature
        project.ext.useXlabMavenPublish =  {
            project.apply plugin: "maven-publish"
            project.ext.xlabPublishing = true
            project.publishing {
                repositories {
                    maven {
                        url "${resolveXlabNexusDomain()}/nexus/content/repositories/${project.version.endsWith("SNAPSHOT") ? "snapshots" : "releases"}"
                        credentials {
                            username 'admin'
                            password 'admin123'
                        }
                    }
                }
                publications {
                    mavenJava(MavenPublication) {
                        from project.components.java
                    }
                }
            }
        }

        //add the awesome xlabCompile scope for dependencies
        project.ext.userXlabDependencyScope = {
            if (project.ext.usingXlabProfiling) {
                project.dependencies.metaClass.xlabCompile = { artifactName ->
                    if (project.ext.developmentMode()) compile project.project(":${artifactName}")
                    else compile "com.wonders.xlab:${artifactName}:+"
                }
                project.dependencies.metaClass.xlabRuntime = { artifactName ->
                    if (project.ext.developmentMode()) runtime project.project(":${artifactName}")
                    else runtime "com.wonders.xlab:${artifactName}:+"
                }
                project.dependencies.metaClass.xlabTestCompile = { artifactName ->
                    if (project.ext.developmentMode()) testCompile project.project(":${artifactName}")
                    else testCompile "com.wonders.xlab:${artifactName}:+"
                }
                project.dependencies.metaClass.xlabTestRuntime = { artifactName ->
                    if (project.ext.developmentMode()) testRuntime project.project(":${artifactName}")
                    else testRuntime "com.wonders.xlab:${artifactName}:+"
                }
                if (project.plugins.hasPlugin('war')) {
                    project.dependencies.metaClass.xlabProvidedCompile = { artifactName ->
                        if (project.ext.developmentMode()) providedCompile project.project(":${artifactName}")
                        else providedCompile "com.wonders.xlab:${artifactName}:+"
                    }
                    project.dependencies.metaClass.xlabProvidedRuntime = { artifactName ->
                        if (project.ext.developmentMode()) providedRuntime project.project(":${artifactName}")
                        else providedRuntime "com.wonders.xlab:${artifactName}:+"
                    }
                }
            }
        }

        //add git push to all remotes task
        def GIT_REMOTE_TAG = "remote"
        project.ext.GIT_REMOTE_TAG = GIT_REMOTE_TAG
        project.ext.useGitBatchPush = {
            project.task("pushAll") {
                List<String> branches = []
                if (project.hasProperty(GIT_REMOTE_TAG)) {
                    for (String branch in (project.property(GIT_REMOTE_TAG).split(','))) {
                        branches.add(branch)
                    }
                }
                if (branches == null|| branches.empty) {
                    def remote = "git remote".execute(null, project.projectDir)
                    remote.waitFor()
                    remote.text.eachLine {
                        branches.add(it)
                    }
                }
                branches.each {
                   "git push $it --all".execute(null, project.projectDir).waitFor()
                }
            }
        }

        //helpers!
        project.ext.useEverything =  {
            project.ext.useXlabMavenRepositories()
            project.ext.useXlabDependencies()
            project.ext.useXlabTesting()
            project.ext.useXlabProfiling()
            project.ext.useConfigGroovy()
            project.ext.useXlabMavenPublish()
            project.ext.useCopyDependencies()
            project.ext.userXlabDependencyScope()
            project.ext.useGitBatchPush()
        }
        project.ext.using = { List<String> keys ->
            for (String key in keys) {
                project.invokeMethod("use${key.capitalize()}", null)
            }
        }
        project.ext.xlabified = project.ext.useEverything
    }

    enum Profile {
        TESTING, DEVELOPMENT, PRODUCTION
    }
}
